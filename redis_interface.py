import redis
from program_state.utils import *
from program_state.redis_config import *


class ProgramStateRedisClient:
    def __init__(self, server_host=REDIS_HOST, server_port=REDIS_PORT, server_password=REDIS_PASS):
        self.host = server_host
        self.port = server_port
        self.password = server_password
        self.conn = None

    def connect(self):
        """
        Connect to server.
        :return: True if success otherwise False.
        """
        try:
            self.conn = redis.Redis(host=self.host, port=self.port, password=self.password)
        except:
            return False
        return True

    def add_program_state(self, state, session_id='none'):
        """
        Add a program state to server.
        :param state: program state
        :param session_id: session id
        :return: True if success otherwise False.
        """
        try:
            state_hash = state.get_md5_hash()
            state_key = f"{session_id}:{state_hash}"
            self.conn.hset(state_key, 'session_id', session_id)
            self.conn.hset(state_key, 'state_hash', state_hash)
            self.conn.hset(state_key, 'state_info', serial_state_to_network(state))
            self.conn.persist(state_key)
        except:
            return False
        return True

    def get_program_state_by_hash(self, state_hash, session_id='none'):
        """
        Get a program state from server by its hash value.
        :param state_hash: hash value
        :param session_id: session id
        :return: state if found, otherwise None.
        """
        state_key = f"{session_id}:{state_hash}"
        serialized_state = self.conn.hget(state_key, 'state_info')
        if serialized_state is None:
            return None
        return revert_state_from_network(serialized_state)

    def replace_old_state_with_new_state(self, old_state, new_state, session_id='none'):
        """
        Replace old state with new state, like merging state...
        :param old_state: old state
        :param new_state: new state
        :param session_id: session id
        :return: True if success otherwise False.
        """
        old_state_hash = old_state.get_md5_hash()
        self.remove_program_state_by_hash(old_state_hash, session_id=session_id)
        return self.add_program_state(state=new_state)

    def remove_program_state_by_hash(self, state_hash, session_id='none'):
        """
        Remove a program state from server by its hash value
        :param state_hash: hash value
        :param session_id: session id
        :return: True if success otherwise False.
        """
        state_key = f"{session_id}:{state_hash}"
        serialized_state = self.conn.hget(state_key, 'state_info')
        if serialized_state is None:
            return True
        try:
            self.conn.delete(state_key)
        except:
            return False
        return True

    def recover_state_chain_by_hash_list(self, hash_list, session_id='none'):
        """
        Recover state chain by a hash list.
        :param hash_list: hash list
        :param session_id: session id
        :return: state chain if success otherwise None.
        """
        state_chain = StateChain()
        for state_hash in hash_list:
            state = self.get_program_state_by_hash(state_hash, session_id=session_id)
            if state is None:
                return None
            state_chain.append_state(state)
        return state_chain

    def store_state_chain_if_needed(self, state_chain, session_id='none'):
        """
        Store all states within a state chain into database.
        :param state_chain: state chain
        :param session_id: session id
        :return: hash_list
        """
        hash_list = []
        for index in range(state_chain.num_states):
            state = state_chain.get_state_by_index(index=index)
            state_hash = state.get_md5_hash()
            hash_list.append(state_hash)
            state_key = f"{session_id}:{state_hash}"
            if self.conn.exists(state_key):
                continue
            self.add_program_state(state, session_id=session_id)
        return hash_list

    def destroy_all_states_by_session_id(self, session_id):
        """
        Delete all states in a session.
        :param session_id: session id
        :return: None
        """
        state_keys = self.conn.keys(f"{session_id}:*")
        for state_key in state_keys:
            try:
                self.conn.delete(state_key)
            except:
                pass
        return

    def get_IP_list_from_state_chain_by_hash_list(self, hash_list, session_id='none'):
        """
        Get IP list from state chain by a given hash list.
        :param hash_list: hash list
        :param session_id: session id
        :return: A list of IP if success otherwise None.
        """
        ip_list = []
        for state_hash in hash_list:
            state = self.get_program_state_by_hash(state_hash, session_id=session_id)
            if state is None:
                return None
            cur_ip_expr = state.ip_expr
            if isinstance(cur_ip_expr, ConstExpression):
                ip_list.append({'absolute': 1, 'type': 'int', 'value': cur_ip_expr.value, 'concrete': 1})
            elif isinstance(cur_ip_expr, OFBExpression):
                offset_expr = cur_ip_expr.offset
                if not isinstance(offset_expr, ConstExpression):
                    ip_list.append({'absolute': 0, 'type': 'json',
                                    'value': {'base': cur_ip_expr.base, 'offset': -1},
                                    'concrete': 1})
                else:
                    ip_list.append({'absolute': 0, 'type': 'json',
                                    'value': {'base': cur_ip_expr.base, 'offset': offset_expr.value},
                                    'concrete': 1})
            else:
                return None
        return ip_list

    def get_IP_info_from_state_by_hash(self, state_hash, session_id='none'):
        """
        Get IP information from a state by a given hash.
        :param state_hash: state hash
        :param session_id: session ID
        :return: IP information if success otherwise None.
        """
        state_key = f"{session_id}:{state_hash}"
        serialized_state = self.conn.hget(state_key, 'state_info')
        if serialized_state is None:
            return None
        state = revert_state_from_network(serialized_state)
        cur_ip_expr = state.ip_expr

        if isinstance(cur_ip_expr, ConstExpression):
            return {'absolute': 1, 'type': 'int', 'value': cur_ip_expr.value, 'concrete': 1}
        elif isinstance(cur_ip_expr, OFBExpression):
            offset_expr = cur_ip_expr.offset
            if not isinstance(offset_expr, ConstExpression):
                return {'absolute': 0, 'type': 'json', 'value': {'base': cur_ip_expr.base, 'offset': -1}, 'concrete': 1}
            else:
                return {'absolute': 0, 'type': 'json', 'value': {'base': cur_ip_expr.base, 'offset': offset_expr.value},
                                'concrete': 1}
        return None

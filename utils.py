from program_state.state import *


def revert_state_from_network(net_data):
    """
    从请求的数据中恢复state
    """
    serialized_state_b64 = net_data
    serialized_state = bytes(base64.b64decode(serialized_state_b64))
    state = pickle.loads(serialized_state)
    return state


def serial_state_to_network(state):
    """
    将给定的state转换为网络数据流
    """
    serialized_state = pickle.dumps(state)
    serialized_state_b64 = str(base64.b64encode(serialized_state), encoding='utf-8')
    return serialized_state_b64


def revert_state_chain_from_network(net_data):
    """
    从请求的数据中恢复state序列
    """
    serialized_state_chain_b64 = net_data
    serialized_state_chain = bytes(base64.b64decode(serialized_state_chain_b64))
    state_chain = pickle.loads(serialized_state_chain)
    return state_chain


def serial_state_chain_to_network(state_chain):
    """
    将给定的state_chain转换为网络数据流
    """
    serialized_state_chain = pickle.dumps(state_chain)
    serialized_new_states_b64 = str(base64.b64encode(serialized_state_chain), encoding='utf-8')
    return serialized_new_states_b64


def pretty_print_state_chain_from_network(net_data):
    state_chain = revert_state_chain_from_network(net_data)
    print(state_chain)

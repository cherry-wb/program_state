# -*- coding: utf-8 -*-

from copy import deepcopy
from program_state.constraint import Constraint
from program_state.expressions import *
import base64
import pickle
import hashlib


class State:
    """
    Basic definition of program state.
    """
    def __init__(self, name="", ip=None):
        """
        :param name: name of this state
        :param ip: RIP register value ( a instant value or OFBExpression)
        :param name(optional): Name of current state.
        """
        self.__name = name
        self.__ip_expr = ip
        self.__constraints = []

    @property
    def constraints(self):
        return self.__constraints

    @property
    def name(self):
        if self.__name == "":
            return "anonymous"
        return self.__name

    @property
    def ip_expr(self):
        def get_ip_from_exprs(vir_expr, another_expr):
            if vir_expr.reg_name not in ['rip', 'ip', 'eip', 'RIP', 'IP', 'EIP']:
                return None
            if isinstance(another_expr, ConstExpression) or isinstance(another_expr, OFBExpression):
                return another_expr
            return None

        if self.__ip_expr is not None:
            return self.__ip_expr

        for _c in self.__constraints:
            found = False
            for expr in _c.expressions:
                if not isinstance(expr, EqExpression):
                    continue

                e1 = expr.sub_exprs[0]
                e2 = expr.sub_exprs[1]
                if isinstance(e1, VIRExpression):
                    new_ip = get_ip_from_exprs(e1, e2)
                elif isinstance(e2, VIRExpression):
                    new_ip = get_ip_from_exprs(e2, e1)
                else:
                    continue
                if new_ip is not None:
                    self.__ip_expr = new_ip
                    found = True
                    break

            if found:
                break

        return self.__ip_expr

    @ip_expr.setter
    def ip_expr(self, value):
        self.__ip_expr = value

    def add_constraint(self, C):
        """
        Add a constraint to current state
        :param c: new constraint
        :return: nothing
        """
        _c = deepcopy(C)  # do not use C directly!
        self.__constraints.append(_c)


    def __str__(self):
        msg = "Name: %s\n" % self.name
        msg += "Constraints: \n"
        for C in self.__constraints:
            msg += "%s" % str(C)
            msg += '\n'
        return msg

    def get_md5_hash(self):
        str_state = str(self)
        return hashlib.md5(str_state.encode(encoding="UTF-8")).hexdigest()


class StateChain:
    def __init__(self):
        self.__state_list = []

    @property
    def num_states(self):
        return len(self.__state_list)

    def get_state_by_index(self, index):
        if index >= self.num_states:
            return None

        return self.__state_list[index]

    def get_state_by_name(self, name):
        for state in self.__state_list:
            if state.name == name:
                return state
        return None

    def append_state(self, S):
        self.__state_list.append(S)

    def merge_state(self, S):
        if self.num_states == 0:
            self.__state_list.append(S)
            return
        for C in S.constraints:
            self.__state_list[-1].add_constraint(C)
        return

    def __str__(self):
        res_str = ''
        for state in self.__state_list:
            res_str += str(state)
        return res_str
